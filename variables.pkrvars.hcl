##################################################################################
# VARIABLES
##################################################################################
floppy_files = ["./cdrom/meta-data", "./cdrom/user-data"]

# Virtual Machine Settings
vm_boot_command             = [
    "<esc><esc><esc>",
    "<enter><wait>",
    "/casper/vmlinuz ",
    "root=/dev/sr0 ",
    "initrd=/casper/initrd ",
    "autoinstall ",
    "ipv6.disable=1 ",
    "<enter>"
]

vm_name                     = "Ubuntu Server 20.04 Template"
vm_guest_os_type            = "ubuntu64Guest"
vm_version                  = 14
vm_firmware                 = "bios"
vm_cdrom_type               = "sata"
vm_cpu_sockets              = 4
vm_cpu_cores                = 1
vm_mem_size                 = 8192
vm_disk_size                = 320000
vm_disk_controller_type     = ["pvscsi"]
vm_network_card             = "vmxnet3"
vm_boot_wait                = "2s"

# ISO Objects
iso_paths                   = ["[HPDS01] .ISOs/Linux/ubuntu-20.04.3-live-server-amd64.iso"]
iso_checksum                = "f8e3086f3cea0fb3fefb29937ab5ed9d19e767079633960ccb50e76153effc98"
iso_urls                    = ["https://releases.ubuntu.com/20.04.3/ubuntu-20.04.3-live-server-amd64.iso"]

# Scripts

shell_scripts               = ["./scripts/generalize.sh"]
